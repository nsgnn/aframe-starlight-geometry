//primitive for bundle of star rays from a point on the surface of the sphere
AFRAME.registerPrimitive('a-subpoint', {
  defaultComponents: {
    subpoint: {},
    starlight: {radius: 1.5, count:50, rayLength: 10}
    },
  mappings: {
    coordinates: 'subpoint.coordinates',
    color: 'subpoint.color',
  }
});

AFRAME.registerComponent('subpoint', {
  schema:{
    //position:           {type: 'vec3', default: {x: 0, y: 0, z: 0}},
    coordinates:           {type: 'vec2', default: {x:0,y:0}}, //in degrees
    color: {type: 'color', default: '#ffffff'},
    direction:{type:'vec3', default:{x:0,y:0,z:0}},
  },
  init: function(){
    var data = this.data;
    this.el.setAttribute('geometry',{primitive:'sphere', radius: .008}, true);
    this.el.setAttribute('material',{color:data.color}, true);

    const globeEntity = document.querySelector('#globe');
    const globeComponent = globeEntity.components.globe;
    scale = globeEntity.components.earth.data.scale;

    data.coordinates = new THREE.Vector2(data.coordinates.x,data.coordinates.y);
    globePosition = globeEntity.object3D.position;
    position = globeComponent.getCoords(data.coordinates.x, data.coordinates.y, 0);
    worldPosition = {x: position.x/scale+globePosition.x, y: position.y/scale+globePosition.y, z: position.z/scale+globePosition.z};
    this.el.object3D.position.set(worldPosition.x,worldPosition.y,worldPosition.z);

    data.direction = globeComponent.getCoords(data.coordinates.x, data.coordinates.y, 0);;
  },
  tick: function (time, deltaTime) {
    //this is quite inefficient
    var data = this.data;
    const globeEntity = document.querySelector('#globe');
    const globeComponent = globeEntity.components.globe;
    scale = globeEntity.components.earth.data.scale;

    globePosition = globeEntity.object3D.position;
    worldPosition = this.el.object3D.position;
    if(this.el.is('dragged')){
      localposition = {x: (worldPosition.x-globePosition.x)/scale, y: (worldPosition.y-globePosition.y)/scale, z: (worldPosition.z-globePosition.z)/scale};
      coordinates = globeComponent.toGeoCoords(localposition);
      data.coordinates.x = coordinates.lat;
      data.coordinates.y = coordinates.lng;
    }
    //set handle on earth surface
    position = globeComponent.getCoords(data.coordinates.x, data.coordinates.y, 0);
    data.direction = position;
    worldPosition = {x: position.x*scale+globePosition.x, y: position.y*scale+globePosition.y, z: position.z*scale+globePosition.z};
    this.el.object3D.position.set(worldPosition.x,worldPosition.y,worldPosition.z);
  }
});


//primitive for bundle of star rays from a point on the surface of the sphere
AFRAME.registerComponent('starlight', {
  schema: {
    count: {type: 'number', default: 1},
    radius: {type: 'number', default: 1},
    rayLength: {type: 'number', default: 1},
    direction:{type:'vec3', default:{x:0,y:0,z:0}},
    position: {type:'vec3', default:{x:0,y:0,z:0}},
    color: {type: 'color', default: '#ffffff'},
  },
  init: function () {
    var data = this.data;
    data.color = this.el.components['subpoint'].data.color;
    data.direction = this.el.components['subpoint'].data.direction;
    data.direction = new THREE.Vector3(data.direction.x, data.direction.y, data.direction.z);
    data.direction.multiplyScalar(data.rayLength);

    this.origins = [];
    for(i=0;i<data.count;i++){
      var randomOrigin = new THREE.Vector3(Math.random()-0.5,Math.random()-0.5, Math.random()-0.5);
      randomOrigin.multiplyScalar(data.radius);
      this.origins.push(randomOrigin);
    }

    const sceneEl = document.querySelector('a-scene');
    const globeEl = document.querySelector('#globe');
    var references = this.references = [];
    for(i=0; i<data.count;i++){
      var entityEl = document.createElement('a-entity');
      entityEl.setAttribute('line', generateLine(data.direction, this.origins[i], data.color));
      entityEl.id = this.el.id + '_starlight_' + i;
      entityEl.object3D.position = globeEl.object3D.position;
      references.push(entityEl.id);
      sceneEl.appendChild(entityEl);
    }
  },
  tick: function (time, deltaTime) {
    var data = this.data;
    //update data as necessary
    data.direction = this.el.components['subpoint'].data.direction;
    data.direction = new THREE.Vector3(data.direction.x-.5, data.direction.y-.5, data.direction.z-.5);
    data.direction.multiplyScalar(data.rayLength);

    var references = this.references;
    for(i=0; i<data.count;i++){
      var entityEl = document.querySelector('#'+references[i]);
      entityEl.setAttribute('line', generateLine(data.direction,this.origins[i], data.color));
    }
  }
});
function isEqualVec3 (a, b) {
  if (!a || !b) { return false; }
  return (a.x === b.x && a.y === b.y && a.z === b.z);
}

function generateLine(direction, origin, color){
    var lineStart = (new THREE.Vector3(origin.x-direction.x,origin.y-direction.y,origin.z-direction.z));
    var lineEnd = (new THREE.Vector3(origin.x+direction.x, origin.y+direction.y, origin.z+direction.z));
  return {start: lineStart, end: lineEnd, color: color};
}
